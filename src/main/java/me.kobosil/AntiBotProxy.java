package me.kobosil;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.logging.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * User: Roman@NullP0interEx
 * Date: 20.11.13
 * Time: 23:04
 * Root: me.kobosil@AntiBotProxy
 */
public class AntiBotProxy {

    public String SOURCE_HOST = "109.230.255.239";
    public int SOURCE_PORT = 25565;
    public String DESTINATION_HOST = "109.230.255.239";
    public int DESTINATION_PORT = 25565;
    public Boolean ONLY_PROXY = true;
    public String KICK_MESSAGE_PING = "[AntiBotProxy] Please add the Server to your Serverlist!";
    public String KICK_MESSAGE_VALIDATE_NAME = "[AntiBotProxy] I can't validate your Username!";
    public String KICK_MESSAGE_MAX_CONNECTIONS = "[AntiBotProxy] You may have exceeded the maximum number of connections to this server.!";
    public String KICK_MESSAGE_BLOCKED_NAME = "[AntiBotProxy] Your name contains: {blocked}";
    public int MAX_CONNECTIONS_IP = 4;
    public ArrayList<String> ipWhitelist = new ArrayList<String>();
    public ArrayList<String> blockedNames = new ArrayList<String>();
    public HashMap<String, ArrayList<String>> ipUserMap = new HashMap<String, ArrayList<String>>();
    public Boolean validateUsernames = false;

    public static final Logger LOGGER = Logger.getLogger("AntiBotProxy");

    public static enum PACKET_TYPE{
        CLIENT, SERVER;
    }

    public static boolean validateUsername(final String username){
        Pattern pattern = Pattern.compile("[A-Za-z0-9_]{2,16}");
        Matcher matcher;
        matcher = pattern.matcher(username);
        return matcher.matches();
    }

    public static byte[] disconnect(String msg)
    {   byte[] reason = new byte[msg.getBytes().length * 2];

        for(int i = 0; i < (reason.length); i++){
            if(i != 0)
                if(i % 2 != 0)
                    reason[i] = msg.getBytes()[i /2];
                   else
                    reason[i] = 0;
        }
        byte[] data = new byte[8192];
        System.arraycopy(reason, 0, data, 3, reason.length);
        data[0] = (byte)255;
        data[2] = (byte)(reason.length /2);
        return data;
    }

    public static String getUsername(byte[] data)
    {
        String username = "";
        String host = "";
        try
        {
        int ulenght = data[3];
        if (ulenght < 3 || ulenght > 16)
            return "null";
        for (int i = 0; i <= ulenght * 2; i++)
        {
            if (data[4 + i] != (byte)0x00)
                username += (char)(data[4 + i]);
        }

        int alenght = data[5 + ulenght * 2];
        for (int i = 0; i <= alenght * 2; i++)
        {
            if (data[(6 + ulenght * 2) + i] != (byte)0x00)
                host += (char)(data[(6 + ulenght * 2) + i]);
        }

        for (int i = 0; i <=  10; i++)
        {
            if (data[(10 + ulenght * 2 + alenght * 2) + i] != (byte)0)
                return "null";
        }
        }
        catch (ArrayIndexOutOfBoundsException ex)
        {
            LOGGER.severe(ex.getMessage());
            return "null";
        }
        //LOGGER.info(username + " --> " + host);
        return username;
    }

    public static void main (String args[]) throws IOException {
        AntiBotProxy proxy = new AntiBotProxy();
        //Create Config file
        if (!new File("config.prop").exists()) {
            writeToFile("from=127.0.0.1:25565");
            writeToFile("to=127.0.0.1:25566");
            writeToFile("backlog=500");
            writeToFile("only_proxy=true");
            writeToFile("KICK_MESSAGE_PING=" + proxy.KICK_MESSAGE_PING);
            writeToFile("KICK_MESSAGE_BLOCKED_NAME=" + proxy.KICK_MESSAGE_BLOCKED_NAME);
            writeToFile("KICK_MESSAGE_VALIDATE_NAME=" + proxy.KICK_MESSAGE_VALIDATE_NAME);
            writeToFile("KICK_MESSAGE_MAX_CONNECTIONS=" + proxy.KICK_MESSAGE_MAX_CONNECTIONS);
            writeToFile("maxConnectionsPerIP=2");
            writeToFile("validateUsernames=true");
            writeToFile("blockedNames=notch;god;admin");
            writeToFile("logFile=proxy.log");
            writeToFile("loggerLevel=INFO");
        }

        //Load file
        HashMap<String, String> conf = loadConfig();

        //Load proxy settings
        proxy.SOURCE_HOST = conf.get("from").split(":")[0];
        proxy.DESTINATION_HOST = conf.get("to").split(":")[0];

        proxy.SOURCE_PORT = Integer.parseInt(conf.get("from").split(":")[1]);
        proxy.DESTINATION_PORT = Integer.parseInt(conf.get("to").split(":")[1]);

        proxy.ONLY_PROXY = Boolean.getBoolean(conf.get("only_proxy"));

        proxy.KICK_MESSAGE_BLOCKED_NAME = conf.get("KICK_MESSAGE_BLOCKED_NAME");
        proxy.KICK_MESSAGE_MAX_CONNECTIONS = conf.get("KICK_MESSAGE_MAX_CONNECTIONS");
        proxy.KICK_MESSAGE_PING = conf.get("KICK_MESSAGE_PING");
        proxy.KICK_MESSAGE_VALIDATE_NAME = conf.get("KICK_MESSAGE_VALIDATE_NAME");

        proxy.MAX_CONNECTIONS_IP = Integer.parseInt(conf.get("maxConnectionsPerIP"));
        proxy.validateUsernames = Boolean.parseBoolean(conf.get("validateUsernames"));
        for(String s :conf.get("blockedNames").split(";"))
            proxy.blockedNames.add(s);

        //Load logging settings
        LOGGER.setUseParentHandlers(false);
        FileHandler fh = new FileHandler(conf.get("logFile"));
        ConsoleHandler ch = new ConsoleHandler();
        fh.setFormatter(new Formatter() {
            public String format(LogRecord record) {
                return "[" + record.getLevel() + "]["
                        + new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(Calendar.getInstance().getTime()) + "] "
                        + record.getMessage() + "\n";
            }
        });
        ch.setFormatter(new Formatter() {
            public String format(LogRecord record) {
                return "[" + record.getLevel() + "]["
                        + new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime()) + "] "
                        + record.getMessage() + "\n";
            }
        });
        LOGGER.addHandler(ch);
        LOGGER.addHandler(fh);
        LOGGER.setLevel(Level.parse(conf.get("loggerLevel")));
        LOGGER.info("Start proxy...");
        ServerSocket serverSocket = new ServerSocket(proxy.SOURCE_PORT, Integer.parseInt(conf.get("backlog")), InetAddress.getByName(proxy.SOURCE_HOST));
        while (true) {
            Socket clientSocket = serverSocket.accept();
            ConnectionThread clientThread = new ConnectionThread(proxy, clientSocket);
            clientThread.start();
        }

    }

    public static HashMap<String, String> loadConfig(){
        HashMap<String, String> conf = new HashMap<String, String>();
        try{
            FileInputStream fstream = new FileInputStream(new File("config.prop"));
            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String line;
            while ((line = br.readLine()) != null){
                conf.put(line.split("=")[0], line.split("=")[1]);
            }
            in.close();
        }catch (Exception e){
            LOGGER.severe("Config Error: " + e.getMessage());
        }
        return conf;
    }

    public static void writeToFile(String text) {
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File("config.prop"), true));
            bw.write(text);
            bw.newLine();
            bw.close();
        } catch (Exception e) {
        }
    }
}
