package me.kobosil;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * User: Roman@NullP0interEx
 * Date: 21.11.13
 * Time: 13:00
 * Root: me.kobosil@AntiBotProxy
 */
public class ConnectionThread extends Thread {
    private Socket clientSocket;
    private Socket remoteSocket;
    private boolean mForwardingActive = false;
    private AntiBotProxy proxy;
    public byte[] nextBuffer = null;

    public ConnectionThread(AntiBotProxy proxy, Socket clientSocket) {
        this.proxy = proxy;
        this.clientSocket = clientSocket;
    }
    
    public void run() {
        InputStream clientInbound;
        OutputStream localOutbound;
        InputStream remoteInbound;
        OutputStream remoteOutbound;
        try {
            // Connect to the destination server
            remoteSocket = new Socket(
                    proxy.DESTINATION_HOST,
                    proxy.DESTINATION_PORT);

            // Turn on keep-alive for both the sockets
            remoteSocket.setKeepAlive(true);
            clientSocket.setKeepAlive(true);

            // Obtain client & server input & output streams
            clientInbound = clientSocket.getInputStream();
            localOutbound = clientSocket.getOutputStream();
            remoteInbound = remoteSocket.getInputStream();
            remoteOutbound = remoteSocket.getOutputStream();
        } catch (IOException ioe) {
            System.err.println("Can not connect to " +
                    proxy.DESTINATION_HOST + ":" +
                    proxy.DESTINATION_PORT);
            connectionBroken();
            return;
        }

        // Start forwarding data between server and client
        mForwardingActive = true;
        ForwarderThread clientForward = new ForwarderThread(proxy, AntiBotProxy.PACKET_TYPE.CLIENT, this, clientInbound, remoteOutbound);
        clientForward.start();
        ForwarderThread serverForward = new ForwarderThread(proxy, AntiBotProxy.PACKET_TYPE.SERVER, this, remoteInbound, localOutbound);
        serverForward.start();

        AntiBotProxy.LOGGER.info("ABP " +
                clientSocket.getInetAddress().getHostAddress() +
                ":" + clientSocket.getPort() + " Connected.");
    }

    public synchronized String getClientIP(){
        return clientSocket.getInetAddress().getHostAddress();
    }

    public synchronized void connectionBroken() {
        try {
            remoteSocket.close();
        } catch (Exception e) {}
        try {
            clientSocket.close(); }
        catch (Exception e) {}

        if (mForwardingActive) {
            AntiBotProxy.LOGGER.info("ABP Forwarding " +
                    clientSocket.getInetAddress().getHostAddress()
                    + ":" + clientSocket.getPort() + " dropped.");
            mForwardingActive = false;
        }
    }
}