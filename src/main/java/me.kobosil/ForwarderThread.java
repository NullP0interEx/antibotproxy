package me.kobosil;


import java.io.*;
import java.util.ArrayList;

/**
 * User: Roman@NullP0interEx
 * Date: 21.11.13
 * Time: 13:03
 * Root: me.kobosil@AntiBotProxy
 */
public class ForwarderThread extends Thread {
    private static final int BUFFER_SIZE = 8192;

    InputStream inputStream;
    OutputStream outputStream;
    ConnectionThread parent;
    AntiBotProxy proxy;
    AntiBotProxy.PACKET_TYPE type;

    public ForwarderThread(AntiBotProxy proxy, AntiBotProxy.PACKET_TYPE type,  ConnectionThread parent, InputStream inputStream, OutputStream outputStream) {
        this.proxy = proxy;
        this.type = type;
        this.parent = parent;
        this.inputStream = inputStream;
        this.outputStream = outputStream;
    }

    public void run() {
        byte[] buffer = new byte[BUFFER_SIZE];
        Boolean run = true;
        try {
            while (run) {
                int bytesRead = inputStream.read(buffer);
                if (bytesRead == -1)
                    break;
                //Bot Detection Start
                if(!proxy.ONLY_PROXY) {
                    if (parent.nextBuffer != null && type == AntiBotProxy.PACKET_TYPE.SERVER) {
                        buffer = parent.nextBuffer;
                        parent.nextBuffer = null;
                    }
                    if (buffer[0] == (byte) 0x00 && type == AntiBotProxy.PACKET_TYPE.CLIENT && !proxy.ipWhitelist.contains(parent.getClientIP())) {
                        AntiBotProxy.LOGGER.info(parent.getClientIP() + " send Ping and has been added to whitelist");
                        proxy.ipWhitelist.add(parent.getClientIP());
                    }

                    if (buffer[0] == (byte) 0xFD && type == AntiBotProxy.PACKET_TYPE.SERVER && !proxy.ipWhitelist.contains(parent.getClientIP())) {
                        AntiBotProxy.LOGGER.info("whitelist doesn't contains " + parent.getClientIP());
                        buffer = AntiBotProxy.disconnect(proxy.KICK_MESSAGE_PING);
                        run = false;
                    }

                    if (buffer[0] == (byte) 0x02 && buffer[1] == (byte) 0x4E && type == AntiBotProxy.PACKET_TYPE.CLIENT) {
                        String username = AntiBotProxy.getUsername(buffer);
                        if (username != "null") {
                            AntiBotProxy.LOGGER.info(username + " send Handshake(0x02)");
                            for (String name : proxy.blockedNames)
                                if (username.toLowerCase().contains(name)) {
                                    parent.nextBuffer = AntiBotProxy.disconnect(proxy.KICK_MESSAGE_BLOCKED_NAME.replace("{blocked}", name));
                                    AntiBotProxy.LOGGER.info("Block blockedNames: " + username);
                                }
                            if (proxy.validateUsernames)
                                if (!AntiBotProxy.validateUsername(username)) {
                                    parent.nextBuffer = AntiBotProxy.disconnect(proxy.KICK_MESSAGE_VALIDATE_NAME);
                                    AntiBotProxy.LOGGER.info("Block validateUsername: " + username);
                                }
                            if (!proxy.ipUserMap.containsKey(parent.getClientIP())) {
                                ArrayList<String> names = new ArrayList<String>();
                                names.add(username);
                                proxy.ipUserMap.put(parent.getClientIP(), names);
                                AntiBotProxy.LOGGER.info(parent.getClientIP() + " add " + username);
                            } else {
                                if (proxy.ipUserMap.get(parent.getClientIP()).size() >= proxy.MAX_CONNECTIONS_IP) {
                                    if (!proxy.ipUserMap.get(parent.getClientIP()).contains(username)) {
                                        parent.nextBuffer = AntiBotProxy.disconnect(proxy.KICK_MESSAGE_MAX_CONNECTIONS);
                                        AntiBotProxy.LOGGER.info("Block MAX_CONNECTIONS_IP: " + username);
                                    }
                                } else if (!proxy.ipUserMap.get(parent.getClientIP()).contains(username)) {
                                    proxy.ipUserMap.get(parent.getClientIP()).add(username);
                                    AntiBotProxy.LOGGER.info(parent.getClientIP() + " add " + username);
                                }
                            }
                        }
                    }
                }
                //Bot Detection Stop
                outputStream.write(buffer, 0, bytesRead);
                outputStream.flush();
            }
        } catch (IOException e) {
        }
        parent.connectionBroken();
    }

    public static String readString(DataInputStream in, int maxSize) throws IOException {
        int length = in.readShort();
        if(length > maxSize)
            throw new IOException("String too big");
        char[] characters = new char[length];
        for(int i = 0; i < length; i++)
            characters[i] = in.readChar();
        return new String(characters);
    }
}
